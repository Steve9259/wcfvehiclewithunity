﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IVehicleDataManager
    {
        bool InsertVehicle(Vehicle obj);

        List<Vehicle> GetAllVehicles();

        Vehicle GetVehicleById(int id);

        bool DeleteVehicle(int id);

        bool UpdateVehicle(Vehicle obj);

    }
}
