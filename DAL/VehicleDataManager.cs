﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class VehicleDataManager : IVehicleDataManager
    {

        public bool InsertVehicle(Vehicle obj)
        {
            SqlHelper.ExecuteNonquery("sp_InsertVehicle", new List<SqlParameter> {
                new SqlParameter("@vehicle_plate",obj.VehiclePlate),
                new SqlParameter("@vehicle_type", obj.VehiclePlate),
                new SqlParameter("@vehicle_year", obj.VehicleYear)
            });
            return true;
        }

        public List<Vehicle> GetAllVehicles()
        {

            // Return a list of Vehicles.
            var myDataTable = SqlHelper.GetDataTable("sp_getAllVehicles");

            var vehicleList = myDataTable.AsEnumerable().Select(dataRow => new Vehicle
            {
                VehicleID = dataRow.Field<int>("VehicleId"),
                VehiclePlate = dataRow.Field<string>("VehiclePlate"),
                VehicleType = dataRow.Field<string>("VehicleType"),
                VehicleYear = dataRow.Field<int>("VehicleYear")
            }).ToList();

            return vehicleList;

        }

        public Vehicle GetVehicleById(int id)
        {

            // Returns Vehicle.
            var myDataTable = SqlHelper.GetDataTable("sp_getVehicleById", new SqlParameter("@Vehicle_id", id));

            var vehicleItem = myDataTable.AsEnumerable().Select(dataRow => new Vehicle
            {
                VehicleID = dataRow.Field<int>("VehicleId"),
                VehiclePlate = dataRow.Field<string>("VehiclePlate"),
                VehicleType = dataRow.Field<string>("VehicleType"),
                VehicleYear = dataRow.Field<int>("VehicleYear")
            }).SingleOrDefault(r => r.VehicleID == id);

            return vehicleItem;

        }

        public bool DeleteVehicle(int id)
        {
            SqlHelper.ExecuteNonquery("sp_deleteVehicleById", new SqlParameter("@id", id));

            return true;
        }

        public bool UpdateVehicle(Vehicle obj)
        {
            SqlHelper.ExecuteNonquery("sp_updateVehicleByID", new List<SqlParameter> {
                new SqlParameter("@id",obj.VehicleID),
                new SqlParameter("@vehicle_plate",obj.VehiclePlate),
                new SqlParameter("@vehicle_type", obj.VehiclePlate),
                new SqlParameter("@vehicle_year", obj.VehicleYear)
            });
            return true;

        }

    }
}
