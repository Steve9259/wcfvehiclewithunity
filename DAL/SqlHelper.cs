﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;

namespace DAL
{
    public class SqlHelper
    {
        #region Sql Object Methods

        public static SqlConnection GetConnection(string connectionString)
        {
            SqlConnection sqlConn = new SqlConnection(connectionString);
            return sqlConn;
        }

        public static SqlConnection GetConnection()
        {
            SqlConnection sqlConn = GetConnection(DefaultConnectionString());
            return sqlConn;
        }

        public static SqlCommand GetCommand(string storedProc, string connectionString)
        {
            SqlCommand sqlCommand = new SqlCommand(storedProc, GetConnection(connectionString));
            return sqlCommand;
        }

        public static SqlCommand GetCommand(string storedProc)
        {
            SqlCommand sqlCommand = GetCommand(storedProc, DefaultConnectionString());
            return sqlCommand;
        }

        public static SqlCommand GetStoredProcCommand(string storedProc, List<SqlParameter> commandParameters, string connectionString)
        {
            SqlCommand sqlCommand = GetCommand(storedProc, connectionString);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            foreach (SqlParameter parameter in commandParameters)
            {
                sqlCommand.Parameters.Add(parameter);
            }
            return sqlCommand;
        }

        public static SqlCommand GetStoredProcCommand(string storedProc, SqlParameter commandParameter, string connectionString)
        {
            SqlCommand sqlCommand = GetCommand(storedProc, connectionString);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add(commandParameter);
            return sqlCommand;
        }

        public static SqlCommand GetStoredProcCommand(string storedProc, string connectionString)
        {
            SqlCommand sqlCommand = GetCommand(storedProc, connectionString);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            return sqlCommand;
        }

        public static SqlCommand GetStoredProcCommand(string storedProc, List<SqlParameter> commandParameters)
        {
            SqlCommand sqlCommand = GetCommand(storedProc);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            foreach (SqlParameter parameter in commandParameters)
            {
                sqlCommand.Parameters.Add(parameter);
            }
            return sqlCommand;
        }

        public static SqlCommand GetStoredProcCommand(string storedProc, SqlParameter commandParameter)
        {
            SqlCommand sqlCommand = GetCommand(storedProc);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add(commandParameter);
            return sqlCommand;
        }

        public static SqlCommand GetStoredProcCommand(string storedProc)
        {
            SqlCommand sqlCommand = GetCommand(storedProc);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            return sqlCommand;
        }

        #endregion


        #region Connection String Methods

        public static string GetConnectionString(string key)
        {
            return "Data Source=DESKTOP-02LC5IH; Initial Catalog=VehicleDB; Integrated Security=True; MultipleActiveResultSets=True";
        }

        private static string DefaultConnectionString()
        {
            return GetConnectionString("SqlDatabase");
        }

        #endregion

        #region ExecuteNonquery

        public static void ExecuteNonquery(string storedProc, SqlParameter commandParameter, string connectionString)
        {
            SqlCommand sqlCommand = GetStoredProcCommand(storedProc, commandParameter, connectionString);
            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();
        }

        public static void ExecuteNonquery(string storedProc, SqlParameter commandParameter)
        {
            SqlCommand sqlCommand = GetStoredProcCommand(storedProc, commandParameter);
            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();
        }

        public static void ExecuteNonquery(string storedProc, List<SqlParameter> commandParameters, string connectionString)
        {
            SqlCommand sqlCommand = GetStoredProcCommand(storedProc, commandParameters, connectionString);
            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();
        }

        public static void ExecuteNonquery(string storedProc, List<SqlParameter> commandParameters)
        {
            SqlCommand sqlCommand = GetStoredProcCommand(storedProc, commandParameters);
            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();
        }

        #endregion


        public static DataTable GetDataTable(string storedProc, SqlParameter commandParameter)
        {
            DataTable dtResults = new DataTable();
            FillDataTable(storedProc, commandParameter, dtResults);
            return dtResults;
        }


        public static DataTable GetDataTable(string storedProc)
        {
            DataTable dtResults = new DataTable();
            FillDataTable(storedProc, dtResults);
            return dtResults;
        }

        public static void FillDataTable(string storedProc, SqlParameter commandParameter, DataTable dataTable)
        {
            SqlCommand sqlCommand = GetStoredProcCommand(storedProc, commandParameter);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            dataAdapter.Fill(dataTable);
            sqlCommand.Connection.Close();
        }

        public static void FillDataTable(string storedProc, DataTable dataTable)
        {
            SqlCommand sqlCommand = GetStoredProcCommand(storedProc);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            dataAdapter.Fill(dataTable);
            sqlCommand.Connection.Close();
        }


    }
}
