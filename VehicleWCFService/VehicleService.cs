﻿using DAL;
using System.Collections.Generic;

namespace VehicleWCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "VehicleService" in both code and config file together.
    public class VehicleService : IVehicleService
    {
        private IVehicleDataManager _vehicleDataManager;

        //public VehicleService()
        //{
        //    _vehicleDataManager = new VehicleDataManager();
        //}

        public VehicleService(IVehicleDataManager vehicleDataManager)
        {
            _vehicleDataManager = vehicleDataManager;
        }

        public List<Vehicle> GetAllVehicles()
        {
           var vehicles = _vehicleDataManager.GetAllVehicles();

            return vehicles;
        }
    }
}
