﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace VehicleWCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IVehicleService" in both code and config file together.
    [ServiceContract]
    public interface IVehicleService
    {
        [OperationContract]
        List<Vehicle> GetAllVehicles();
    }
}
